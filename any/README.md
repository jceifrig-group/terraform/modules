## `any` Modules
This directory contains Terraform modules that are provider-independent: they
do not require any other provider-specific resources.