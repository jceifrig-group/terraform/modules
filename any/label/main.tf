# The `label` module is implemented almost entirely as a bunch of `local`
# definitions. The module doesn't instantiate any resources, just performs a
# bunch of calculations to collect any parent attributes, system attributes,
# implicit attributes, and extra attributes into an overall set, and then
# deriving an ID from them.

locals {
  # ---------------------- Calculate 's_attributes' -------------------------
  # Collect the 'system' attributes into a map.  System attributes are the ones
  # that are not defined explicitly in the module instantation (or its parent)
  # but are extracted from the Terraform instantiation itself.
  system_attrs = {for k, v in {
      # The default Terraform namespace is 'default', and rather than having a
      # bunch of resources named 'default-A-B-C', it's better to suppress the
      # workspace portion of those IDs:
      workspace = terraform.workspace == "default" ? "" : terraform.workspace
    } : k => v if v != null
  }

  # Collect the 'implicit' attributes into a map.  Implicit attributes are the
  # ones that correspond to a corresponding module input variable. Undefined
  # variables are stripped out so we don't have a bunch of nulls clogging up
  # our map.
  implicit_attrs = { for k, v in {
      environment = var.environment
      namespace   = var.namespace
      stage       = var.stage
      instance    = var.instance
      id_format   = var.id_format
    } : k => v if v != null
  }

  # The 'extra' attributes are the ones given as explicit name/value attributes
  # in the 'extra_attrs' module variable. Strip out any keys being mapped to
  # 'null', in case somebody tried to slip us a Mikey....
  extra_attrs = { for k, v in var.extra_attrs : k => v if v != null }

  # Merge together all of the attribute maps into one overall map, paying
  # attention to the precedence order we're going for.
  # Remember, 'merge'-ed keys on the right override keys set on the left!
  s_attributes = merge(var.context.s_attributes, local.system_attrs,
                       local.extra_attrs, local.implicit_attrs)

  # --------------------- Calculate 'ls_attributes' -------------------------
  # The 'ls_attributes' of a label are list-of-strings attributes.  They're
  # ad-hoc, internal-use-only attributes used when generating the 'context'
  # of the label (to fake label inheritance).  Currently, only one such
  # attribute is defined: 'tag_keys', the list of label attributes used when
  # generating a tag map:
  base_tag_keys = coalesce(var.tags, var.context.ls_attributes["tag_keys"])

  ls_attributes = var.context.ls_attributes

  # Now we're ready to start calculating derived features of our Label.
  id_format = local.s_attributes["id_format"]

  # Generate the 'id' property of the label by using the `id_format` property
  # as a template.  First, extract out all of the keys used in the template;
  # the keys appear in the template enclosed in '<>' delimiters.
  format_keys = flatten(regexall("<(\\w+)>", local.id_format))
  # We might not have definitions for all these keys in our s_attributes map,
  # so we need to extend that with a value for the missing keys:
  key_map = merge({ for k in local.format_keys : k => ""}, local.s_attributes)

  # Substitute the values of the keys for the key names in the id template.
  # There is no excuse for the contorted way this is done: the "<...>" delimiters
  # are replaced with "|...|", the string then split on the pipes into fragments,
  # the fragments matching keys then substituted, and the result reassembled into
  # a string.  The only excuse is that Terraform has an extremely limited set of
  # functions to work with, and no looping construct of note.
  id_prefrags = split("|", replace(local.id_format, "/<(\\w+)>/", "|$1|"))
  id_postfrags = [for f in local.id_prefrags :
                    contains(local.format_keys, f) ? local.key_map[f] : f
                 ]
  id_precleanup = join("", local.id_postfrags)

  # Cleanup the id string.  There are a couple of things that need to be patched
  # up. If an ID component is missing, it will have been replaced by an empty
  # string, which means that there may be leading/trailing/repeated delimiters
  # that need to be cleaned up. Sadly, Terraform REs don't support back
  # references, so we have to separately for each delimiter character:
  id_cleanup_1 = replace(local.id_precleanup, "/-+/", "-")
  id = replace(local.id_cleanup_1, "/(^-)|(-$)/", "")

  # Calculate a set of tags.  The 'tag_keys' list attribute give the list of
  # the attributes we want to extract from our overall list of attributes. We'll
  # strip out any undefined keys from our list of tags to reduce the choking
  # hazard of using the tags elsewhere:
  pre_tags = { for k in local.ls_attributes["tag_keys"] :
           k => contains(keys(local.s_attributes), k) ? local.s_attributes[k] : null
         }
  tags = { for k, v in local.pre_tags: k => v if v != null}

  # Set our output values:
  output_id = local.id
  output_tags = local.tags
  output_context = {
    s_attributes = local.s_attributes
  }
}