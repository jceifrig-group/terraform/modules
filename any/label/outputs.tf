output "context" {
  value = local.output_context
  description = "The label context generated from this label."
}

output "id" {
  value = local.output_id
}

output "tags" {
  value = local.output_tags
}