# 'context' - An encoding of the parent label of this label instance. Terraform
# modules aren't OOP classes, so an actual module instance can't be bound here
# to a "label instance".  Instead, the internal data structure of a `label`
# module can be retrieved as the module's `context` and used as the basis for
# instantiating new `label` instances.  Obviously, the structure of the
# 'context' variable has to be maintained in sync with the structure of the
# 'context' output of the module.
#
# Also, here is where the 'default' values of a label's attributes are set.
# If a particular attribute of a label isn't set, it is supposed to be inherited
# from its parent label (if any), not set to some default value common to all
# labels. The 'default' context supplies default values to all attributes for
# 'root' labels without parents.
variable "context" {
  type = object({
    s_attributes = map(string)
    ls_attributes = map(list(string))
  })
  description = "The base label context for instantiating this label."
  default = {
    s_attributes = {
      id_format = "<workspace>-<namespace>-<environment>-<instance>"
    }
    ls_attributes = {
      tag_keys = []
    }
  }
}

# For brevity and as a bit of protection from misspelling tag names, we define
# a bunch of convenience variables for `label`s; these are the 'implicit'
# attributes referred to in the embedded comments of the module implementation.
# These will all be folded into the overall list of attributes of a `label`, but
# are simpler to define when instantiating the module. These attributes have no
# special meaning; their descriptions are just their intended use.

variable environment  {
  type = string
  default = null
  description = "The 'environment' attribute of the label, such as 'prod' or 'aws'"
}
variable "id_format" {
  type = string
  default = null
  description = "The label attributes to use when generating the label 'id', such as '<workspace>-<namespace>-<environment>-<instance>'"
}

variable "namespace" {
  type = string
  default = null
  description = "The 'namespace' attribute of the label, such as an org, or a system name."
}

variable "stage" {
  type = string
  default = null
  description = "The 'stage' attribute of the label, such as 'test' or 'build'"
}

variable "instance" {
  type = string
  default = null
  description = "A discriminator for multiple instances of the same resource type, such as '1', 'master', etc."
}

variable "extra_attrs" {
  type = map(string)
  default = {}
  description = "Any additional ad-hoc string-valued attributes can be set here."
}

variable "tags" {
  type = list(string)
  default = null
  description = "The list of label attribute keys used to generate a tag set. (Overrides parent list.)"
}

variable "extra_tags" {
  type = list(string)
  default = null
  description = "A list of additional label attribute keys used to generate a tag set. (Aguments parent list.)"
}