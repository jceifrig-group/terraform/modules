# `label` - Generate a consistent resource name and tag set

The `label` resource is designed to make it easier to name and tag your
resources in a consistent way. As your infrastructure grows and consists of
more and more resources, keeping track of these resources becomes a greater
and greater challenge. If resources are given *ad hoc* names it becomes
difficult to trace resources back to the Terraform stack that they belong to.
Establishing naming and tagging conventions solves this problem, but enforcing
the convention manually is tedious and error-prone. To make this easier, use
the `label` module.

A `label` is a generalization of a set of name/value tags, with a few additional
features:
- `label`-s have features derived from their tag set that make it easy generate
resource names based on these labels in a consistent way.
- A `label` has an optional parent, from which it inherits all the parent's tags.
By putting common tags in a parent label, resources that have a label with a
common parent can be assigned names that will automatically share these common
features, making it easier to identify related resources.

The `label` module is loosely based on/inspired by Cloud Posse's
[terraform-null-label](https://github.com/cloudposse/terraform-null-label)
module. Many thanks for a great idea!